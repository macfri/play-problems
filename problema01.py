import random


def run():

    l = []

    for x in range(100):

        while True:
            n = random.randint(1, 10000)

            if n not in l:
                l.append(n)
                break

    max = 0
    min = l[0]

    for x in l:
        max = x if x > max else max
        min = x if x < min else min

    print {'values': l, 'max': max, 'min': min}


if __name__ == '__main__':

    run()
