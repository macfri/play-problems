def run():

    data = {}

    words = open('alfabeto.txt', 'r')
    words = words.read().split('\n')[:-1]

    def find_words(value):

        if not value:
            return False

        for x in value:
            if not x in words:
                return False

        return True

    input = open('rfc2616.txt', 'r')
    input = input.read().split(' ')
    input = [x for x in input if find_words(x)]

    for x in input:

        if not x in data:
            data[x] = 1
        else:
            data[x] = data[x] + 1

    new_data = {}

    for x, y in data.items():
        if y >= 100:
            new_data[x] = y

    print new_data

if __name__ == '__main__':

    run()
