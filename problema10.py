import os
import sys
import random


def run():

    input = open('hangman.txt', 'r')
    input = input.read().split('\n')[:-1]

    n = 8
    chr = '*'

    phase = ''
    phase1 = input[random.randint(0, len(input) - 1)]
    phase_original = phase1

    #print 'phase_original: %s' % phase_original

    while 0 < n:

        n -= 1

        if phase in phase1:
            phase1 = phase1.replace(phase, chr * len(phase))

        output = ''
        for y, x in enumerate(phase1):

            if x == chr:
                output += phase_original[y]
            elif x == ' ':
                output += ' '
            else:
                output += chr

        if output == phase_original:
            print 'excelent'
            break
        else:
            if n == 0:
                print 'so bad'
                break

        print 'phase: %s' % output
        phase = raw_input('Enter phase: ')
        sys.stdout.write(os.popen('clear').read())

if __name__ == '__main__':

    print run()
