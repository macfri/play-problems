import sys

if __name__ == '__main__':

    args = sys.argv

    if len(args) == 2:

        check = True
        nro = args[1]

        if nro == '01':
            import problema01 as problem
        elif nro == '02':
            import problema02 as problem
        elif nro == '03':
            import problema03 as problem
        elif nro == '04':
            import problema04 as problem
        elif nro == '06':
            import problema06 as problem
        elif nro == '07':
            import problema07 as problem
        elif nro == '08':
            import problema08 as problem
        elif nro == '09':
            import problema09 as problem
        elif nro == '10':
            import problema10 as problem
        else:
            check = False
            print 'Nro: %s is incorrect' % nro

        if check:
            problem.run()
