import random


def run():

    placas = []

    input = open('prefijos_placas.txt', 'r')
    input = input.read().split('\n')[:-1]

    for p in range(100):

        while True:
            code = input[random.randint(0, len(input) - 1)]
            serie = random.randint(100, 999)
            placa = '%s-%d' % (code, serie)

            if placa not in placas:
                placas.append(placa)
                break

    print placas


if __name__ == '__main__':

    run()
