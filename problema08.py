import os
import sys
import time
import random


def run():

    input = open('matrix.txt', 'r')
    input = input.read().split('\n')[:-1]

    while True:

        body = ''

        for x in range(8):

            chr = input[random.randint(0, len(input) - 1)]
            body += '%s  ' % chr

        sys.stdout.write(os.popen('clear').read())
        print body
        time.sleep(2)

if __name__ == '__main__':

    print run()
